import numpy as np
import HW1_1 as f
import matplotlib.pyplot as plt

'''
Parameters
'''
phi = np.array([1.,0.95,-0.5])
sigma = 1
T = 50
N = 10
y_ = np.array([0.,0.])
epsilon = np.random.rand(T+1)

##############
# Part 1. b. #
##############
y = f.generate_y(phi,sigma,y_,T,epsilon)
print(y)

##############
# Part 1. d. #
##############
x_ = np.hstack([y_,1.])
A,C = f.generate_AC(phi,sigma)
x = f.generate_x(A,C,x_,T,epsilon)
print(x)

gen_yt = x[0, ::]
assert ((y == gen_yt).all())


##############
# Part 1. e. #
##############
yt = f.generate_N_y(phi,sigma,y_,500,T)
A,C = f.generate_AC(phi,sigma)
xhat = f.generate_xhat(A,x_,T)
ybar = np.mean(yt, axis=0)
print("They follow the same pattern, however obviously ybar is greater because it includes a residual. Xhat is the value of X, without the residual. However, both have the same parameters so their growth is the same, but y includes the randomized residuals.")
Y_i = plt.plot(ybar, 'r')
OLS = plt.plot(xhat[0,:], 'b')
plt.xlabel('t')
plt.show()

