import numpy as np

def generate_y(phi,sigma,y_,T,epsilon):
    r'''
    Generates :math:`y_t` following 
        
        :math:`y_t = \phi_0+\phi_1y_{t-1}+\phi_2 y_{t-2} + \sigma \epsilon_t`

    
    Inputs
    -----------
    * phi (list or array) : [phi_0,phi_1,phi_2]
    
    * sigma (float) : :math:'\sigma`
    
    * y_ (list or array) : :math:`[y_{-1},y_{-2}]`
    
    * T (int) : number of periods to simulate
    
    * epsilon (length T+1 numpy array) : :math:`[\epsilon_0,\epsilon_1,...,\epsilon_T]`
    
    
    
    Returns
    -----------
    * y(length T+1 numpy array) : :math:`[y_0,y_1,...,y_T]`
    '''

    y_ = y_[::-1]
    y = np.ones((T+1))
    for i in range(T+1):
        result = phi[0] + (phi[1]*y_[-1]) + (phi[2]*y_[-2]) + sigma*epsilon[i]
        y_ = np.array([y_[-1], result])
        y[i] = result
    return y



def generate_N_y(phi,sigma,y_,N,T):
    r'''
    Generates N realizations of y^i_t following 

        :math:`y^i_t = \phi_0+\phi_1y^i_{t-1}+\phi_2 y^i_{t-2} + \sigma \epsilon^i_t`
    
    Inputs
    -----------
    * phi (list or array) : [phi_0,phi_1,phi_2]
    
    * sigma (float) : :math:`\sigma`
    
    * y_ (list or array) : :math:`[y_{-1},y_{-2}]`
    
    * N (int) : number of simulations
    
    * T (int) : number of periods to simulate
    
    
    Returns
    -----------
    * y(Nx(T+1) numpy array) : y[i,t] is the realization of y at time t for simulation i
    '''
    y = np.ones((N, T+1))
    for n in range(N):
        epsilon = np.random.rand(T+1)
        simulation = generate_y(phi,sigma,y_,T,epsilon)
        y[n] = simulation
    return y

def generate_AC(phi,sigma):
    r'''
    Create A,C for the process
    
        :math:`x_{t} = A x_{t-1} + C\epsilon_{t}`
    
    Inputs
    -----------
    * phi (list or array) : [phi_0,phi_1,phi_2]
    
    * sigma (float) : :math:`\sigma`
    
    
    Returns
    -----------
    * A ( :math:`(3\times 3)` array)
    * C (length 3 array)
    '''
    A = np.array([[phi[1], phi[2], phi[0]], [1, 0, 0], [0, 0, 1]])
    C = np.array([sigma, 0, 0])
    A = np.rot90(A, 3)
    A = np.fliplr(A)
    return A, C

def generate_x(A,C,x_,T,epsilon):
    r'''
    Generates :math:`x_t` following 
        
        :math:`x_{t} = A x_{t-1} + C\epsilon_{t}`

    
    Inputs
    -----------
    * A (:math:`m\times m` array)
    
    * C (length m array)
    
    * x_ (length m array) : :math:`x_{-1}`
    
    * T (int) : number of periods to simulate
    
    * epsilon (length T+1 numpy array) : :math:`[\epsilon_0,\epsilon_1,...,\epsilon_T]`
    
    
    Returns
    -----------
    * x(length :math:`m\times(T+1) numpy array) : x[:,t] is the array :math:`x_t`
    '''
    x = np.ones(((T+1), len(A)))
    for i in range(0, T+1):
        t = x_.dot(A)
        t[0] += epsilon[i]
        x_ = t
        x[i] = t
    x = np.rot90(x, 3)
    x = np.fliplr(x)
    return x

def generate_xhat(A,xhat_,T):
    r'''
    Generates :math:`\hat x_t` following 
        
        :math:`\hat x_{t} = A \hat x_{t-1}`

    
    Inputs
    -----------
    * A (:math:`m\times m` array)
    
    * xhat_ (length m array) : :math:`\hat x_{-1}`
    
    * T (int) : number of periods to simulate
    
    
    Returns
    -----------
    * xhat(length :math:`m\times(T+1) numpy array) : xhat[:,t] is the array 
    :math:`\hat x_t`
    '''
    xhat = np.ones((T+1, len(A)))
    for i in range(T+1):
        t = xhat_.dot(A)
        xhat[i] = t
        xhat_ = t
    xhat = np.rot90((xhat), 3)
    xhat = np.fliplr(xhat)
    return xhat




